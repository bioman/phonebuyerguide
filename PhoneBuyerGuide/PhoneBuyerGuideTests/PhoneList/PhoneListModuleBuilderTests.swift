//
//  PhoneListModuleBuilderTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneListModuleBuilderTests: XCTestCase {

    func testContactListBuilder() {
        // when
        let viewController = PhoneListModuleBuilder.build()

        // then
        guard let navigationController = viewController as? UINavigationController else {
            return XCTFail("phone list should be embedded in a navigation controller")
        }
        XCTAssertEqual(navigationController.viewControllers.count, 1)
        XCTAssertNotNil(navigationController.viewControllers.first as? PhoneListTableViewController)
    }

}
