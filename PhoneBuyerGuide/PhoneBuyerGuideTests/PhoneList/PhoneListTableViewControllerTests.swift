//
//  PhoneListTableViewControllerTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneListTableViewControllerTests: XCTestCase {

    private var viewController: PhoneListTableViewController!
    private var output: MockPhoneListViewOutput!

    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        viewController = storyBoard.instantiateViewController(withIdentifier: "PhoneListTableViewController") as? PhoneListTableViewController
        output = MockPhoneListViewOutput()

        viewController.output = output
        XCTAssertNotNil(viewController.view)
    }

    override func tearDown() {
        output = nil
        viewController = nil
        super.tearDown()
    }

    func testViewIsReady() {
        // when
        viewController.viewDidLoad()

        // then
        XCTAssertTrue(output.isViewReadyCalled)
    }

    func testReloadDataCalled() {
        // given
        let refreshControl = UIRefreshControl()

        // when
        viewController.handleRefresh(refreshControl)

        // then
        XCTAssertTrue(output.reloadDataCalled)
    }

    func testSelectPageTypeAll() {
        // give
        let segmentedControl = UISegmentedControl(items: ["All", "Fav"])
        segmentedControl.selectedSegmentIndex = PageType.all.rawValue

        // when
        viewController.segmentedTapped(segmentedControl)

        // then
        XCTAssertTrue(output.selectPageTypeCalled)
        XCTAssertEqual(output.selectedPageType, .all)
    }

    func testSelectPageTypeFavorite() {
        // give
        let segmentedControl = UISegmentedControl(items: ["All", "Fav"])
        segmentedControl.selectedSegmentIndex = PageType.favorite.rawValue

        // when
        viewController.segmentedTapped(segmentedControl)

        // then
        XCTAssertTrue(output.selectPageTypeCalled)
        XCTAssertEqual(output.selectedPageType, .favorite)
    }

    func testSortButtonTap() {
        // when
        viewController.sortButtonTapped(self)

        // then
        XCTAssertTrue(output.sortButtonTappedCalled)
    }

    func testSelectCellViewModel() {
        // given
        let indexPath = IndexPath(row: 2, section: 0)
        let mockCellViewModels = MockPhoneListModule.mockPhoneCellViewModels()
        viewController.reloadView(with: mockCellViewModels)

        // when
        viewController.tableView(viewController.tableView, didSelectRowAt: indexPath)

        // then
        XCTAssertTrue(output.selectCellViewModelCalled)
        XCTAssertEqual(output.selectCellViewModel, mockCellViewModels[indexPath.row])
    }
}

final class MockPhoneListViewOutput: PhoneListViewOutput & PhoneCellOutput {

    var isViewReadyCalled = false
    func viewIsReady() {
        isViewReadyCalled = true
    }

    var reloadDataCalled = false
    func reloadData() {
        reloadDataCalled = true
    }

    var selectPageTypeCalled = false
    var selectedPageType: PageType = .all
    func select(pageType: PageType) {
        selectPageTypeCalled = true
        selectedPageType = pageType
    }

    var sortButtonTappedCalled = false
    func sortButtonTapped() {
        sortButtonTappedCalled = true
    }

    var selectCellViewModelCalled = false
    var selectCellViewModel: PhoneCellViewModel = MockPhoneListModule.mockPhoneCellViewModel(phoneId: 1)
    func select(cellViewModel: PhoneCellViewModel) {
        selectCellViewModelCalled = true
        selectCellViewModel = cellViewModel
    }

    func markFavorite(phoneId: Int64) {}
    func removeFavorite(phoneId: Int64) {}
}
