//
//  MockPhoneListModuleBuilder.swift
//  PhonesTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 23/2/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import UIKit

final class MockPhoneListModule {
    static func mockPhoneCellViewModels() -> [PhoneCellViewModel] {
        return [
            mockPhoneCellViewModel(phoneId: 1),
            mockPhoneCellViewModel(phoneId: 2),
            mockPhoneCellViewModel(phoneId: 3)
        ]
    }

    static func mockPhoneItems() -> [PhoneItem] {
        return [
            mockPhoneItem(phoneId: 1),
            mockPhoneItem(phoneId: 2),
            mockPhoneItem(phoneId: 3)
        ]
    }

    static func mockPhoneCellViewModel(
        phoneId: Int64,
        price: Double? = 399.0,
        rating: Double? = 8.5,
        isFavorite: Bool? = false) -> PhoneCellViewModel {
        return PhoneCellViewModel(
            phoneId: 1,
            imageURL: "https://www.test.com",
            name: "Phone",
            description: "Phone XXX",
            price: price ?? 0.0,
            rating: rating ?? 0.0,
            isFavorite: isFavorite ?? false
        )
    }

    static func mockPhoneItem() -> PhoneItem {
        return PhoneItem(
            phoneId: 99,
            thumbImageURL: "https://www.test.com",
            name: "iPhone",
            brand: "Apple",
            price: 399.0,
            rating: 8.5,
            description: "iPhone XXX"
        )
    }

    static func mockPhoneItem(phoneId: Int64) -> PhoneItem {
        return PhoneItem(
            phoneId: phoneId,
            thumbImageURL: "https://www.test.com",
            name: "Phone",
            brand: "Apple",
            price: 399.0,
            rating: 8.5,
            description: "iPhone XXX"
        )
    }

    static func mockSortOptionsModel() -> SortOptions.Model {
        return SortOptions.Model(
            title: "Sort",
            message: "",
            priceLowToHighCaption: "Low to high",
            priceHighToLowCaption: "High to low",
            ratingCaption: "Rating",
            cancelCaption: "Cancel"
        )
    }
}

final class MockPhoneListViewInput: PhoneListViewInput {
    var reloadViewCalled = false
    var reloadCellViewModels: [PhoneCellViewModel]? = nil
    func reloadView(with cellViewModels: [PhoneCellViewModel]?) {
        reloadViewCalled = true
        reloadCellViewModels = cellViewModels
    }
}

final class MockPhoneListInteractorInput: PhoneListInteractorInput {
    var fetchPhoneListCalled = false
    func fetchPhoneList() {
        fetchPhoneListCalled = true
    }

    var markFavoriteCalled = false
    func markFavorite(phoneId: Int64) {
        markFavoriteCalled = true
    }

    var removeFavoriteCalled = false
    func removeFavorite(phoneId: Int64) {
        removeFavoriteCalled = true
    }

}

final class MockPhoneListRouterInput: PhoneListRouterInput {

    var presentPhoneDetailCalled = false
    func presentPhoneDetail(with cellViewModel: PhoneCellViewModel) {
        presentPhoneDetailCalled = true
    }

    var presentSortOptionsAlertCalled = false
    func presentSortOptionsAlert(model: SortOptions.Model, completion: @escaping SortOptions.Completion) {
        presentSortOptionsAlertCalled = true
    }
    var presentErrorCalled = false
    func presentError(error: Error) {
        presentErrorCalled = true
    }
}

final class MockPhoneListInteractorOutput: PhoneListInteractorOutput {

    var fetchPhoneListSuccessCalled = false
    func fetchPhoneListSuccess(with items: [PhoneItem]?, favoriteList: [Int64]?) {
        fetchPhoneListSuccessCalled = true
    }

    var fetchPhoneListFailedCalled = false
    func fetchPhoneListFailed(with error: Error) {
        fetchPhoneListFailedCalled = true
    }
}
