//
//  PhoneListRouterTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneListRouterTests: XCTestCase {

    private var router: PhoneListRouter!
    private var viewController: MockViewController!

    override func setUp() {
        super.setUp()
        viewController = MockViewController()
        let navigationController = MockNavigationController(rootViewController: viewController)
        viewController.mockNavigationController = navigationController
        router = PhoneListRouter()
        router.viewController = viewController
    }

    override func tearDown() {
        viewController = nil
        router = nil

        super.tearDown()
    }

    func testPushPhoneDetail() {
        // given
        let cellViewModel = MockPhoneListModule.mockPhoneCellViewModel(phoneId: 99)

        // when
        router.presentPhoneDetail(with: cellViewModel)

        // then
        guard let navigationController = viewController.navigationController as? MockNavigationController else {
            return XCTFail("Navigation controller should not be nil")
        }
        XCTAssertTrue(navigationController.pushCalled)
        XCTAssertTrue(navigationController.viewControllerToPush?.isKind(of: PhoneDetailViewController.self) ?? false)
    }
}
