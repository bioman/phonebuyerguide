//
//  PhoneListInteractorTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneListInteractorTests: XCTestCase {
    var interactor: PhoneListInteractor!
    var output: MockPhoneListInteractorOutput!
    var service: MockPhoneService!

    override func setUp() {
        super.setUp()
        interactor = PhoneListInteractor()
        output = MockPhoneListInteractorOutput()
        service = MockPhoneService()
        interactor.output = output
        interactor.service = service
    }

    override func tearDown() {
        service = nil
        output = nil
        interactor = nil
        super.tearDown()
    }

    func testFetchPhonesSuccess() {
        // given
        service.mockFetchPhoneListSuccess = true

        // when
        interactor.fetchPhoneList()

        // then
        XCTAssertTrue(output.fetchPhoneListSuccessCalled)
    }

    func testFetchPhonesFailed() {
        // given
        service.mockFetchPhoneListSuccess = false

        // when
        interactor.fetchPhoneList()

        // then
        XCTAssertTrue(output.fetchPhoneListFailedCalled)
    }
}
