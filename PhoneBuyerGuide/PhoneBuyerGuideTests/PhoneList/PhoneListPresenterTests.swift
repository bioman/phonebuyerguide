//
//  PhoneListPresenterTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneListPresenterTests: XCTestCase {
    private var presenter: PhoneListPresenter!
    private var mockView: MockPhoneListViewInput!
    private var mockInteractor: MockPhoneListInteractorInput!
    private var mockRouter: MockPhoneListRouterInput!

    override func setUp() {
        presenter = PhoneListPresenter()
        mockView = MockPhoneListViewInput()
        mockInteractor = MockPhoneListInteractorInput()
        mockRouter = MockPhoneListRouterInput()

        presenter.view = mockView
        presenter.interactor = mockInteractor
        presenter.router = mockRouter

    }

    override func tearDown() {
        mockRouter = nil
        mockInteractor = nil
        mockView = nil
        presenter = nil
        super.tearDown()
    }

    func testReloadViewCalledWhenFecthListSuccess() {
        // given
        let items = MockPhoneListModule.mockPhoneItems()
        let favoriteList = [Int64(1), Int64(2)]

        // when
        presenter.fetchPhoneListSuccess(with: items, favoriteList: favoriteList)

        // then
        XCTAssertTrue(mockView.reloadViewCalled)
        XCTAssertEqual(mockView.reloadCellViewModels?.count, 3)
        XCTAssertTrue(mockView.reloadCellViewModels?[0].isFavorite ?? false)
        XCTAssertTrue(mockView.reloadCellViewModels?[1].isFavorite ?? false)
        XCTAssertFalse(mockView.reloadCellViewModels?[2].isFavorite ?? true)
    }

    func testNumberOfItemWhenTapFavoriteScreen() {
        // given
        let items = MockPhoneListModule.mockPhoneItems()
        let favoriteList = [Int64(1), Int64(2)]

        // when
        presenter.fetchPhoneListSuccess(with: items, favoriteList: favoriteList)
        presenter.select(pageType: .favorite)

        // then
        XCTAssertTrue(mockView.reloadViewCalled)
        XCTAssertEqual(mockView.reloadCellViewModels?.count, 2)
        XCTAssertTrue(mockView.reloadCellViewModels?[0].isFavorite ?? false)
        XCTAssertTrue(mockView.reloadCellViewModels?[1].isFavorite ?? false)
    }

    func testFetchPhoneListCalled() {
        // when
        presenter.viewIsReady()

        // then
        XCTAssertTrue(mockInteractor.fetchPhoneListCalled)
    }

    func testReloadData() {
        // when
        presenter.reloadData()

        // then
        XCTAssertTrue(mockInteractor.fetchPhoneListCalled)
    }

    func testPresentPhoneDetailCalled() {
        // given
        let cellViewModel = MockPhoneListModule.mockPhoneCellViewModel(phoneId: 5)

        // when
        presenter.select(cellViewModel: cellViewModel)

        // then
        XCTAssertTrue(mockRouter.presentPhoneDetailCalled)
    }

    func testPresentSortAlertControllerCalled() {
        // when
        presenter.sortButtonTapped()

        // then
        XCTAssertTrue(mockRouter.presentSortOptionsAlertCalled)
    }

    func testPresentErrorAlertControllerCalled() {
        // given
        let error = NSError(domain: "UT", code: 9999, userInfo: nil)

        // when
        presenter.fetchPhoneListFailed(with: error)

        // then
        XCTAssertTrue(mockRouter.presentErrorCalled)
    }

    func testMarkFavorite() {
        // given
        let phoneId: Int64 = 1

        // when
        presenter.markFavorite(phoneId: phoneId)

        // then
        XCTAssertTrue(mockInteractor.markFavoriteCalled)
    }

    func testRemoveFavorite() {
        // given
        let phoneId: Int64 = 1

        // when
        presenter.removeFavorite(phoneId: phoneId)

        // then
        XCTAssertTrue(mockInteractor.removeFavoriteCalled)
    } 
}
