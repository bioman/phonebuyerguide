//
//  MockPhoneService.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class MockPhoneService: PhoneService {
    var mockPhones: [PhoneItem] = MockPhoneListModule.mockPhoneItems()
    var mockFetchPhoneListSuccess = true
    func fetchPhoneList(completion: @escaping PhoneServiceCompletion) {
        if mockFetchPhoneListSuccess {
            completion(Result.success(mockPhones))
        } else {
            completion(Result.failure(MockPhoneBuyerGuide.mockError))
        }
    }

    var mockImages: [ImageItem] = MockPhoneDetailModule.mockImageItems()
    var mockFetchPhoneDetailSuccess = true
    func fetchPhoneDetail(parameter: PhoneDetailRequestParameter, completion: @escaping PhoneDetailServiceCompletion) {
        if mockFetchPhoneDetailSuccess {
            completion(Result.success(mockImages))
        } else {
            completion(Result.failure(MockPhoneBuyerGuide.mockError))
        }
    }
}
