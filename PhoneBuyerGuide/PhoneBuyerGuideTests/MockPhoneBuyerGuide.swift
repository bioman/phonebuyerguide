//
//  MockPhoneBuyerGuide.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import Foundation

final class MockPhoneBuyerGuide {
    static var mockError: Error {
        return NSError(domain: "UT", code: 9999, userInfo: nil)
    }
}
