//
//  MockViewController.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class MockViewController: UIViewController {
    var mockNavigationController: UINavigationController?
    override var navigationController: UINavigationController? {
        return mockNavigationController
    }

    var dismissCalled = false
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dismissCalled = flag
        completion?()
    }

    var presentCalled = false
    var viewControllerToPresent: UIViewController?
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        presentCalled = true
        self.viewControllerToPresent = viewControllerToPresent
        completion?()
    }
}

final class MockNavigationController: UINavigationController {
    var presentCalled = false
    var viewControllerToPresent: UIViewController?
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        presentCalled = true
        self.viewControllerToPresent = viewControllerToPresent
        completion?()
    }

    var pushCalled = false
    var viewControllerToPush: UIViewController?
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushCalled = true
        self.viewControllerToPush = viewController
    }
}
