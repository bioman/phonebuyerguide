//
//  MockPhoneDetailModule.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import UIKit

final class MockPhoneDetailModule {
    static func mockImageItems() -> [ImageItem] {
        return [
            ImageItem(mobileId: 1, imageId: 1, imageURL: "https://www.test.com"),
            ImageItem(mobileId: 2, imageId: 2, imageURL: "https://www.test.com"),
            ImageItem(mobileId: 3, imageId: 3, imageURL: "https://www.test.com")
        ]
    }
}

final class MockPhoneDetailViewInput: PhoneDetailViewInput {
    var setupViewWithCellViewModelCalled = false
    func setupView(with cellViewModel: PhoneCellViewModel?) {
        setupViewWithCellViewModelCalled = true
    }

    var setupViewWithItems = false
    func setupView(with items: [ImageItem]?) {
        setupViewWithItems = true
    }
}

final class MockPhoneDetailInteractorInput: PhoneDetailInteractorInput {
    var fetchPhoneDetailCalled = false
    func fetchPhoneDetail(with phoneId: Int64) {
        fetchPhoneDetailCalled = true
    }
}

final class MockPhoneDetailRouterInput: PhoneDetailRouterInput {
    var presentErrorCalled = false
    func presentError(error: Error) {
        presentErrorCalled = true
    }
}

final class MockPhoneDetailInteractorOutput: PhoneDetailInteractorOutput {
    var fetchPhoneDetailSuccessCalled = false
    func fetchPhoneDetailSuccess(with items: [ImageItem]?) {
        fetchPhoneDetailSuccessCalled = true
    }

    var fetchPhoneDetailFailedCalled = false
    func fetchPhoneDetailFailed(with error: Error) {
        fetchPhoneDetailFailedCalled = true
    }
}
