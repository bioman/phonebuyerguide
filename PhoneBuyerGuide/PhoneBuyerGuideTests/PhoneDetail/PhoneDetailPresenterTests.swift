//
//  PhoneDetailPresenterTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneDetailPresenterTests: XCTestCase {
    private var presenter: PhoneDetailPresenter!
    private var mockView: MockPhoneDetailViewInput!
    private var mockInteractor: MockPhoneDetailInteractorInput!
    private var mockRouter: MockPhoneDetailRouterInput!

    override func setUp() {
        presenter = PhoneDetailPresenter()
        mockView = MockPhoneDetailViewInput()
        mockInteractor = MockPhoneDetailInteractorInput()
        mockRouter = MockPhoneDetailRouterInput()

        presenter.view = mockView
        presenter.interactor = mockInteractor
        presenter.router = mockRouter
    }

    override func tearDown() {
        mockRouter = nil
        mockInteractor = nil
        mockView = nil
        presenter = nil
        super.tearDown()
    }

    func testViewDidLoad() {
        // given
        presenter.cellViewModel = MockPhoneListModule.mockPhoneCellViewModel(phoneId: 2)

        // when
        presenter.viewIsReady()

        // then
        XCTAssertTrue(mockView.setupViewWithCellViewModelCalled)
        XCTAssertTrue(mockInteractor.fetchPhoneDetailCalled)
    }

    func testReloadViewCalledWhenFecthImageListSuccess() {
        // given
        let items = MockPhoneDetailModule.mockImageItems()

        // when
        presenter.fetchPhoneDetailSuccess(with: items)

        // then
        XCTAssertTrue(mockView.setupViewWithItems)
    }

    func testReloadViewCalledWhenFecthImageListFailed() {
        // given
        let error = NSError(domain: "UT", code: 9999, userInfo: nil)

        // when
        presenter.fetchPhoneDetailFailed(with: error)

        // then
        XCTAssertTrue(mockRouter.presentErrorCalled)
    }
}
