//
//  PhoneDetailInteractorTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneDetailInteractorTests: XCTestCase {
    var interactor: PhoneDetailInteractor!
    var output: MockPhoneDetailInteractorOutput!
    var service: MockPhoneService!

    override func setUp() {
        super.setUp()
        interactor = PhoneDetailInteractor()
        output = MockPhoneDetailInteractorOutput()
        service = MockPhoneService()
        interactor.output = output
        interactor.service = service
    }

    override func tearDown() {
        service = nil
        output = nil
        interactor = nil
        super.tearDown()
    }

    func testFetchPhonesSuccess() {
        // given
        let mockPhoneId: Int64 = 1
        service.mockFetchPhoneDetailSuccess = true

        // when
        interactor.fetchPhoneDetail(with: mockPhoneId)

        // then
        XCTAssertTrue(output.fetchPhoneDetailSuccessCalled)
    }

    func testFetchPhonesFailed() {
        // given
        let mockPhoneId: Int64 = 1
        service.mockFetchPhoneDetailSuccess = false

        // when
        interactor.fetchPhoneDetail(with: mockPhoneId)

        // then
        XCTAssertTrue(output.fetchPhoneDetailFailedCalled)
    }
}
