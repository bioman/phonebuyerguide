//
//  PhoneDetailModuleBuilderTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneDetailModuleBuilderTests: XCTestCase {

    func testPhoneDetailModuleBuilder() {
        // given
        let cellViewModel = MockPhoneListModule.mockPhoneCellViewModel(phoneId: 3)

        // when
        let viewController = PhoneDetailModuleBuilder.build(with: cellViewModel)

        // then
        XCTAssertNotNil(viewController as? PhoneDetailViewController)
    }

}
