//
//  PhoneDetailViewControllerTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class PhoneDetailViewControllerTests: XCTestCase {

    private var viewController: PhoneDetailViewController!
    private var output: MockPhoneDetailViewOutput!

    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        viewController = storyBoard.instantiateViewController(withIdentifier: "PhoneDetailViewController") as? PhoneDetailViewController
        output = MockPhoneDetailViewOutput()

        viewController.output = output
        XCTAssertNotNil(viewController.view)
    }

    override func tearDown() {
        output = nil
        viewController = nil
        super.tearDown()
    }

    func testViewIsReady() {
        // when
        viewController.viewDidLoad()

        // then
        XCTAssertTrue(output.isViewReadyCalled)
    }

}

final class MockPhoneDetailViewOutput: PhoneDetailViewOutput {

    var isViewReadyCalled = false
    func viewIsReady() {
        isViewReadyCalled = true
    }
}
