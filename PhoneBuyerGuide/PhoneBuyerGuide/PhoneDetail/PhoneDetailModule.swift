//
//  PhoneDetailModule.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

protocol PhoneDetailViewInput: class {
    func setupView(with cellViewModel: PhoneCellViewModel?)
    func setupView(with items: [ImageItem]?)
}

protocol PhoneDetailViewOutput {
    func viewIsReady()
}

protocol PhoneDetailInteractorInput: class {
    func fetchPhoneDetail(with phoneId: Int64)
}

protocol PhoneDetailInteractorOutput: class {
    func fetchPhoneDetailSuccess(with items: [ImageItem]?)
    func fetchPhoneDetailFailed(with error: Error)
}

protocol PhoneDetailRouterInput {
    func presentError(error: Error)
}
