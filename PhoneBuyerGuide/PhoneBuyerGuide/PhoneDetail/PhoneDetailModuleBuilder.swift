
//
//  PhoneDetailModuleBuilder.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class PhoneDetailModuleBuilder {
    static func build(with cellViewModel: PhoneCellViewModel) -> UIViewController {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "PhoneDetailViewController")

        guard let viewController = vc as? PhoneDetailViewController else {
            fatalError("PhoneDetailViewController not found")
        }
        let router = PhoneDetailRouter()
        router.viewController = viewController

        let presenter = PhoneDetailPresenter()
        presenter.cellViewModel = cellViewModel
        presenter.view = viewController
        presenter.router = router

        let interactor = PhoneDetailInteractor()
        interactor.service = PhoneServiceImplementation()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        return viewController
    }

    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
