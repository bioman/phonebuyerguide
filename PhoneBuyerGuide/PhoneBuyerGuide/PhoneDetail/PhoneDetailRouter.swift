//
//  PhoneDetailRouter.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class PhoneDetailRouter: PhoneDetailRouterInput {
    weak var viewController: UIViewController?

    func presentError(error: Error) {
        let alertController = UIAlertController(title: "Whoops", message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.viewController?.present(alertController, animated: true)
        }
    }
}
