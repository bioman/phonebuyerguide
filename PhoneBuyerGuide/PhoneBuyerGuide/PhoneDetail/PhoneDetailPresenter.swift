//
//  PhoneDetailPresenter.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import Foundation

final class PhoneDetailPresenter {
    weak var view: PhoneDetailViewInput?
    var interactor: PhoneDetailInteractorInput?
    var router: PhoneDetailRouterInput?
    var cellViewModel: PhoneCellViewModel?
}

extension PhoneDetailPresenter: PhoneDetailViewOutput {
    func viewIsReady() {
        guard let cellViewModel = cellViewModel else { return }
        view?.setupView(with: cellViewModel)
        interactor?.fetchPhoneDetail(with: cellViewModel.phoneId)
    }
}

extension PhoneDetailPresenter: PhoneDetailInteractorOutput {
    func fetchPhoneDetailSuccess(with items: [ImageItem]?) {
        view?.setupView(with: items)
    }

    func fetchPhoneDetailFailed(with error: Error) {
        router?.presentError(error: error)
    }
}
