//
//  PhoneDetailViewController.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class PhoneDetailViewController: UIViewController {

    @IBOutlet private weak var imageScrollView: UIScrollView!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UITextView!
    var output: PhoneDetailViewOutput?

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewIsReady()
    }
}

private extension PhoneDetailViewController {
    func configureViews(with cellViewModel: PhoneCellViewModel) {
        title = cellViewModel.name
        priceLabel.text = String(format: "Price: $%.02f", cellViewModel.price)
        ratingLabel.text = String(format: "Rating: %.01f", cellViewModel.rating)
        descriptionLabel.text = cellViewModel.description
    }

    func configureImageViews(with items: [ImageItem]) {
        let imageWidth = imageScrollView.bounds.width
        let imageHeight = imageScrollView.bounds.height
        for (index, item) in items.enumerated() {
            let rect = CGRect(
                x: CGFloat(index) * imageWidth,
                y: CGFloat(0),
                width: imageWidth,
                height: imageHeight
            )
            let imageView = UIImageView(frame: rect)
            imageView.image(
                from: item.imageURL,
                placeholderImage: UIImage(named: "placeholder_image")
            )
            imageView.contentMode = .scaleAspectFit
            imageScrollView.addSubview(imageView)
        }
        imageScrollView.contentSize = CGSize(
            width: CGFloat(items.count) * imageWidth,
            height: imageHeight
        )
    }
}

extension PhoneDetailViewController: PhoneDetailViewInput {
    func setupView(with cellViewModel: PhoneCellViewModel?) {
        guard let cellViewModel = cellViewModel else { return }
        configureViews(with: cellViewModel)
    }

    func setupView(with items: [ImageItem]?) {
        guard let items = items else { return }
        DispatchQueue.main.async {
            self.configureImageViews(with: items)
        }
    }
}
