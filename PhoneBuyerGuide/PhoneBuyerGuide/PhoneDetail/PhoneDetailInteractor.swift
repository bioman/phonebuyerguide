//
//  PhoneDetailInteractor.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import Foundation

final class PhoneDetailInteractor: PhoneDetailInteractorInput {
    weak var output: PhoneDetailInteractorOutput?
    var service: PhoneService?

    func fetchPhoneDetail(with phoneId: Int64) {
        let parameter = PhoneDetailRequestParameter(mobileId: phoneId)
        service?.fetchPhoneDetail(parameter: parameter, completion: { [weak self] result in
            switch result {
            case .success(let items):
                self?.output?.fetchPhoneDetailSuccess(with: items)
            case .failure(let error):
                self?.output?.fetchPhoneDetailFailed(with: error)
            }
        })
    }
}
