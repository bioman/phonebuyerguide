//
//  PhoneService.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import Foundation

typealias PhoneServiceCompletion = (Result<[PhoneItem]?>) -> Void
typealias PhoneDetailServiceCompletion = (Result<[ImageItem]?>) -> Void

protocol PhoneService {
    func fetchPhoneList(completion: @escaping PhoneServiceCompletion)
    func fetchPhoneDetail(parameter: PhoneDetailRequestParameter, completion: @escaping PhoneDetailServiceCompletion)
}

final class PhoneServiceImplementation: PhoneService {
    let baseURL = "https://scb-test-mobile.herokuapp.com/"
    let path = "api/mobiles/"
    let imagePath = "%li/images/"

    func fetchPhoneList(completion: @escaping PhoneServiceCompletion) {
        guard let url = URL(string: baseURL + path) else { return }

        let loadDataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                let response = response as? HTTPURLResponse
                if response?.statusCode != 200 {
                    print("Response failed. status code: \(String(describing: response?.statusCode))")
                }
                guard let error = error else { return }
                return completion(Result.failure(error))
            }

            let phones = try? JSONDecoder().decode([PhoneItem].self, from: data)
            completion(Result.success(phones))
        }
        loadDataTask.resume()
    }

    func fetchPhoneDetail(parameter: PhoneDetailRequestParameter, completion: @escaping PhoneDetailServiceCompletion) {
        let formattedImagePath = String(format: imagePath, parameter.mobileId)
        guard let url = URL(string: baseURL + path + formattedImagePath) else { return }

        let loadDataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                let response = response as? HTTPURLResponse
                if response?.statusCode != 200 {
                    print("Response failed. status code: \(String(describing: response?.statusCode))")
                }
                guard let error = error else { return }
                return completion(Result.failure(error))
            }

            let images = try? JSONDecoder().decode([ImageItem].self, from: data)
            completion(Result.success(images))
        }
        loadDataTask.resume()
    }
}
