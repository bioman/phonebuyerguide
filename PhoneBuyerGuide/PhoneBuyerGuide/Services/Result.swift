//
//  Result.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

public enum Result<Value> {
    case success(Value)
    case failure(Error)
}

public extension Result {

    public init(_ error: Error) {
        self = .failure(error)
    }

    public init(_ value: Value) {
        self = .success(value)
    }

    public var isSuccess: Bool {
        switch self {
        case .success:
            return true

        case .failure:
            return false
        }
    }

    public var value: Value? {
        switch self {
        case .success(let value):
            return value

        case .failure:
            return nil
        }
    }

    public var error: Error? {
        switch self {
        case .success:
            return nil

        case .failure(let error):
            return error
        }
    }
}
