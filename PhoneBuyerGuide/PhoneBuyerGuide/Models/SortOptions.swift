//
//  UIAlertController+SortOptions.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

struct SortOptions {

    enum SortOptionDecision {
        case priceLowToHigh
        case priceHighToLow
        case rating
        case cancel
    }

    struct Model: Equatable {
        let title: String
        let message: String
        let priceLowToHighCaption: String
        let priceHighToLowCaption: String
        let ratingCaption: String
        let cancelCaption: String

        init(title: String,
             message: String,
             priceLowToHighCaption: String,
             priceHighToLowCaption: String,
             ratingCaption: String,
             cancelCaption: String) {
            self.title = title
            self.message = message
            self.priceLowToHighCaption = priceLowToHighCaption
            self.priceHighToLowCaption = priceHighToLowCaption
            self.ratingCaption = ratingCaption
            self.cancelCaption = cancelCaption
        }
    }

    typealias Completion = (SortOptionDecision) -> Void

    static func sortOptions(model: Model,
                             completion: Completion?) -> UIAlertController {
        let alert = UIAlertController(
            title: model.title,
            message: model.message,
            preferredStyle: .alert
        )
        let priceLowToHighAction = UIAlertAction(title: model.priceLowToHighCaption, style: .default) {  _ in
            completion?(.priceLowToHigh)
        }
        alert.addAction(priceLowToHighAction)
        let priceHighToLowAction = UIAlertAction(title: model.priceHighToLowCaption, style: .default) {  _ in
            completion?(.priceHighToLow)
        }
        alert.addAction(priceHighToLowAction)
        let ratingAction = UIAlertAction(title: model.ratingCaption, style: .default) {  _ in
            completion?(.rating)
        }
        alert.addAction(ratingAction)
        let cancelAction = UIAlertAction(title: model.cancelCaption, style: .cancel) {  _ in
            completion?(.cancel)
        }
        alert.addAction(cancelAction)
        return alert
    }

}
