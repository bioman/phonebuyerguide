//
//  PhoneListPresenter.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import Foundation

final class PhoneListPresenter {
    weak var view: PhoneListViewInput?
    var interactor: PhoneListInteractorInput?
    var router: PhoneListRouterInput?
    var items: [PhoneItem]?
    private var viewModels: [PhoneCellViewModel]?
    private var currentPageType: PageType = .all

}

extension PhoneListPresenter: PhoneListViewOutput {
    func viewIsReady() {
        interactor?.fetchPhoneList()
    }

    func reloadData() {
        interactor?.fetchPhoneList()
    }

    func select(pageType: PageType) {
        switch pageType {
        case .all: showAll()
        case .favorite: showFavorite()
        }
    }

    func select(cellViewModel: PhoneCellViewModel) {
        router?.presentPhoneDetail(with: cellViewModel)
    }

    func sortButtonTapped() {
        let sortOptionsCompletion: SortOptions.Completion = { [weak self] decision in
            switch decision {
            case .priceLowToHigh:
                self?.sortPriceLowToHigh()
            case .priceHighToLow:
                self?.sortPriceHighToLow()
            case .rating:
                self?.sortRating()
            case .cancel:
                break
            }
        }
        router?.presentSortOptionsAlert(
            model: SortOptions.Model(
                title: "Sort",
                message: "",
                priceLowToHighCaption: "Price low to high",
                priceHighToLowCaption: "Price high to low",
                ratingCaption: "Rating",
                cancelCaption: "Cancel"),
            completion: sortOptionsCompletion
        )
    }
}

// MARK: - Sort & Filter functions
private extension PhoneListPresenter {
    func showAll() {
        currentPageType = .all
        reloadView(with: viewModels)
    }

    func showFavorite() {
        currentPageType = .favorite
        reloadView(with: viewModels)
    }

    func filterFavorite() -> [PhoneCellViewModel]? {
        return viewModels?.filter({ $0.isFavorite == true })
    }

    func sortPriceLowToHigh() {
        viewModels?.sort { $0.price < $1.price }
        reloadView(with: viewModels)
    }

    func sortPriceHighToLow() {
        viewModels?.sort { $0.price > $1.price }
        reloadView(with: viewModels)
    }

    func sortRating() {
        viewModels?.sort { $0.rating > $1.rating }
        reloadView(with: viewModels)
    }

    func reloadView(with viewModels: [PhoneCellViewModel]?) {
        if currentPageType == .favorite {
            view?.reloadView(with: filterFavorite())
        } else {
            view?.reloadView(with: viewModels)
        }
    }
}

extension PhoneListPresenter: PhoneListInteractorOutput {
    func fetchPhoneListSuccess(with items: [PhoneItem]?, favoriteList: [Int64]?) {
        guard let items = items else { return }
        let viewModels = PhoneCellViewModelFactory.buildCellViewModels(
            from: items,
            favoriteList: favoriteList
        )
        self.viewModels = viewModels
        reloadView(with: viewModels)
    }

    func fetchPhoneListFailed(with error: Error) {
        router?.presentError(error: error)
    }
}

extension PhoneListPresenter: PhoneCellOutput {
    func markFavorite(phoneId: Int64) {
        viewModels?.filter{ $0.phoneId == phoneId }.first?.isFavorite = true
        interactor?.markFavorite(phoneId: phoneId)
    }

    func removeFavorite(phoneId: Int64) {
        viewModels?.filter{ $0.phoneId == phoneId }.first?.isFavorite = false
        interactor?.removeFavorite(phoneId: phoneId)
        if currentPageType == .favorite {
            showFavorite()
        }
    }
}
