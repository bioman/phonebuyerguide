//
//  PhoneListModuleBuider.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class PhoneListModuleBuilder {
    static func build() -> UIViewController {
        let navViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationViewController")

        if let navViewController = navViewController as? UINavigationController,
            let view = navViewController.viewControllers.first as? PhoneListTableViewController {
            let router = PhoneListRouter()
            router.viewController = view

            let presenter = PhoneListPresenter()
            presenter.view = view
            presenter.router = router

            let interactor = PhoneListInteractor()
            interactor.service = PhoneServiceImplementation()
            interactor.userDefault = UserDefaults.standard
            interactor.output = presenter

            presenter.interactor = interactor
            view.output = presenter
        }
        return navViewController
    }

    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
