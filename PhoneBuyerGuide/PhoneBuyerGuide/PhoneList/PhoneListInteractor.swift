//
//  PhoneListInteractor.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import Foundation

final class PhoneListInteractor: PhoneListInteractorInput {
    weak var output: PhoneListInteractorOutput?
    var service: PhoneService?
    var userDefault: UserDefaults?

    func fetchPhoneList() {
        service?.fetchPhoneList(completion: { [weak self] result in
            switch result {
            case .success(let items):
                let favoriteList = self?.userDefault?.array(forKey: Constants.favoriteListKey) as? [Int64]
                self?.output?.fetchPhoneListSuccess(with: items, favoriteList: favoriteList)
            case .failure(let error):
                self?.output?.fetchPhoneListFailed(with: error)
            }
        })
    }

    func markFavorite(phoneId: Int64) {
        if var favoriteList = userDefault?.array(forKey: Constants.favoriteListKey) as? [Int64] {
            if !favoriteList.contains(phoneId) {
                favoriteList.append(phoneId)
                userDefault?.set(favoriteList, forKey: Constants.favoriteListKey)
            }
        } else {
            let favoriteList: Array<Int64> = [phoneId]
            userDefault?.set(favoriteList, forKey: Constants.favoriteListKey)
        }
    }

    func removeFavorite(phoneId: Int64) {
        if var favoriteList = userDefault?.array(forKey: Constants.favoriteListKey) as? [Int64] {
            if favoriteList.contains(phoneId) {
                if let index = favoriteList.index(of: phoneId) {
                    favoriteList.remove(at: index)
                    userDefault?.set(favoriteList, forKey: Constants.favoriteListKey)
                }
            }
        }
    }
}
