//
//  PhoneListTableViewController.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class PhoneListTableViewController: UITableViewController {

    typealias PhoneListOutput = PhoneListViewOutput & PhoneCellOutput

    var output: PhoneListOutput?
    private var cellViewModels: [PhoneCellViewModel] = []

    @IBOutlet weak var segmentedControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        output?.viewIsReady()
    }

    private func setupTableView() {
        tableView.tableFooterView = UIView()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(
            self,
            action: #selector(handleRefresh(_:)),
            for: UIControl.Event.valueChanged
        )
        refreshControl?.layoutIfNeeded()
        refreshControl?.beginRefreshing()
    }
    @IBAction func segmentedTapped(_ sender: UISegmentedControl) {
        output?.select(pageType: PageType.init(rawValue: sender.selectedSegmentIndex) ?? .all)
    }

    @IBAction func sortButtonTapped(_ sender: Any) {
        output?.sortButtonTapped()
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        output?.reloadData()
        refreshControl.beginRefreshing()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PhoneCell.reuseIdentifier) else {
            fatalError("Can not dequeue cell")
        }

        if let cell = cell as? PhoneCell {
            let cellViewModel = cellViewModels[indexPath.row]
            cell.configure(
                with: cellViewModel,
                pageType: PageType(rawValue: segmentedControl.selectedSegmentIndex) ?? .all
            )
            cell.output = output
        }
        return cell
    }

    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output?.select(cellViewModel: cellViewModels[indexPath.row])
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if let pageType = PageType.init(rawValue: segmentedControl.selectedSegmentIndex),
            pageType == .favorite {
            return true
        }
        return false
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let phoneId = cellViewModels[indexPath.row].phoneId
            output?.removeFavorite(phoneId: phoneId)
        }
    }
}

extension PhoneListTableViewController: PhoneListViewInput {
    func reloadView(with cellViewModels: [PhoneCellViewModel]?) {
        guard let cellViewModels = cellViewModels else { return }
        self.cellViewModels = cellViewModels
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
}
