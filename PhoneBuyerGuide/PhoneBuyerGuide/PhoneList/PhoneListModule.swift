//
//  PhoneListModule.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

enum PageType: Int {
    case all
    case favorite
}

struct Constants {
    static let favoriteListKey = "FAVORITE_LIST"
}

extension PageType: RawRepresentable {
    public typealias RawValue = Int

    init?(rawValue: Int) {
        switch rawValue {
        case 0: self = .all
        case 1: self = .favorite
        default: return nil
        }
    }

    public var rawValue: Int {
        switch self {
        case .all: return 0
        case .favorite: return 1
        }
    }
}

protocol PhoneListViewInput: class {
    func reloadView(with cellViewModels: [PhoneCellViewModel]?)
}

protocol PhoneListViewOutput {
    func viewIsReady()
    func reloadData()
    func select(pageType: PageType)
    func sortButtonTapped()
    func select(cellViewModel: PhoneCellViewModel)
}

protocol PhoneListInteractorInput: class {
    func fetchPhoneList()
    func markFavorite(phoneId: Int64)
    func removeFavorite(phoneId: Int64)
}

protocol PhoneListInteractorOutput: class {
    func fetchPhoneListSuccess(with items: [PhoneItem]?, favoriteList: [Int64]?)
    func fetchPhoneListFailed(with error: Error)
}

protocol PhoneListRouterInput {
    func presentPhoneDetail(with cellViewModel: PhoneCellViewModel)
    func presentError(error: Error)
    func presentSortOptionsAlert(model: SortOptions.Model,
                            completion: @escaping SortOptions.Completion)
}
