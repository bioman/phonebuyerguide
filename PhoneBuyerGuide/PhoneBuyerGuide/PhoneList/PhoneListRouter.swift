//
//  PhoneListRouter.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class PhoneListRouter: PhoneListRouterInput {
    weak var viewController: UIViewController?

    func presentPhoneDetail(with cellViewModel: PhoneCellViewModel) {
        let detailVC = PhoneDetailModuleBuilder.build(with: cellViewModel)
        viewController?.navigationController?.pushViewController(detailVC, animated: true)
    }

    func presentError(error: Error) {
        let alertController = UIAlertController(title: "Whoops", message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.viewController?.present(alertController, animated: true)
        }
    }

    func presentSortOptionsAlert(model: SortOptions.Model, completion: @escaping SortOptions.Completion) {
        let alert = SortOptions.sortOptions(model: model, completion: completion)
        viewController?.navigationController?.present(alert, animated: true, completion: nil)
    }
}
