//
//  UIImageView+.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

extension UIImageView {
    @nonobjc
    func image(from urlString: String, placeholderImage: UIImage? = nil) {
        let url = URL(string: urlString)
        image(from: url, placeholderImage: placeholderImage)
    }

    @nonobjc
    func image(from url: URL?, placeholderImage: UIImage? = nil) {
        if let placeholderImage = placeholderImage {
            self.image = placeholderImage
        }
        guard let url = url else { return }

        let cache =  URLCache.shared
        let request = URLRequest(url: url)
        DispatchQueue.global(qos: .userInitiated).async {
            if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.image = image
                }
            } else {
                URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data, let response = response,
                        ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300,
                        let image = UIImage(data: data) {
                        let cachedData = CachedURLResponse(response: response, data: data)
                        cache.storeCachedResponse(cachedData, for: request)
                        DispatchQueue.main.async {
                            self.image = image
                        }
                    }
                }).resume()
            }
        }
    }
}
